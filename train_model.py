import numpy as np
import os

import sklearn
from tensorflow.keras.callbacks import TensorBoard


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
from PIL import Image
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten, Dropout, Conv2D, MaxPool2D
from tensorflow.keras.activations import sigmoid, tanh, relu
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.losses import mean_squared_error
from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.losses import categorical_crossentropy
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import tensorflow.keras as keras


class MachineLearning:
    categories = [
        "Action",
        "Ecchi",
        "Fantasy",
        "Slice Of Life",
    ]

    accepted_values = [
        0.65,
        0.65,
        0.65,
        0.65,
    ]

    target_resolution = (225, 315)

    def load_dataset(self):
        ximgs = []
        y_train = []

        for idx, category in enumerate(self.categories):
            for file in os.listdir(f"./dataset/train/{category}/"):
                ximgs.append(np.array(
                    Image.open(f"./dataset/train/{category}/{file}").resize(self.target_resolution).convert('RGB')) / 255.0)
                temp = [0] * 4
                temp[idx] = 1
                y_train.append(temp)

        ximgs_test = []
        y_test = []
        for idx, category in enumerate(self.categories):
            for file in os.listdir(f"./dataset/test/{category}/"):
                ximgs_test.append(np.array(
                    Image.open(f"./dataset/test/{category}/{file}").resize(self.target_resolution).convert('RGB')) / 255.0)
                temp = [0] * 4
                temp[idx] = 1
                y_test.append(temp)
        x_train = np.array(ximgs)
        y_train = np.array(y_train)

        x_test = np.array(ximgs_test)
        y_test = np.array(y_test)

        return (x_train, y_train), (x_test, y_test)

    def create_linear_model(self):
        model = Sequential()
        model.add(Flatten())
        model.add(Dense(4, activation=sigmoid))
        model.compile(optimizer=SGD(), loss=mean_squared_error, metrics=['accuracy'])
        return model

    def create_mlp_model(self):
        model = Sequential()
        model.add(Flatten())
        model.add(Dense(225, activation=tanh))
        model.add(Dense(315, activation=tanh))
        model.add(Dense(4, activation=sigmoid))
        model.compile(optimizer=SGD(), loss=binary_crossentropy, metrics=['accuracy'])
        return model

    def create_cnn_model(self):
        model = Sequential()

        model.add(Conv2D(4, (3, 3), padding='same', activation=relu))
        model.add(MaxPool2D((2, 2)))

        model.add(Conv2D(8, (3, 3), padding='same', activation=relu))
        model.add(MaxPool2D((2, 2)))

        model.add(Conv2D(16, (3, 3), padding='same', activation=relu))
        # model.add(MaxPool2D((2, 2)))

        model.add(Flatten())
        model.add(Dense(64, activation=tanh))
        model.add(Dense(4, activation=sigmoid))
        model.compile(optimizer=SGD(), loss=mean_squared_error, metrics=['accuracy'])
        return model

    def create_rnn_model(self):
        input_tensor = keras.layers.Input((self.target_resolution[0], self.target_resolution[1], 3))

        previous_tensor = Flatten()(input_tensor)

        next_tensor = Dense(64, activation=relu)(previous_tensor)

        previous_tensor = keras.layers.Concatenate()([previous_tensor, next_tensor])

        next_tensor = Dense(64, activation=relu)(previous_tensor)

        previous_tensor = keras.layers.Concatenate()([previous_tensor, next_tensor])

        next_tensor = Dense(64, activation=relu)(previous_tensor)

        previous_tensor = keras.layers.Concatenate()([previous_tensor, next_tensor])

        next_tensor = Dense(64, activation=tanh)(previous_tensor)
        next_tensor = Dense(4, activation=sigmoid)(next_tensor)

        model = keras.models.Model(input_tensor, next_tensor)

        model.compile(optimizer=SGD(), loss=mean_squared_error, metrics=['accuracy'])
        return model

    def hamming_score(self, y_true, y_pred, normalize=True, sample_weight=None):
        acc_list = []
        for i in range(y_true.shape[0]):
            set_true = set(np.where(y_true[i])[0])
            set_pred = set(np.where(y_pred[i])[0])

            tmp_a = None
            if len(set_true) == 0 and len(set_pred) == 0:
                tmp_a = 1
            else:
                tmp_a = len(set_true.intersection(set_pred)) / \
                        float(len(set_true.union(set_pred)))

            acc_list.append(tmp_a)
        return np.mean(acc_list)

    def round_preds(self, preds):
        for line in preds:
            has_value = False
            # for index, column in enumerate(line):
            #     if line[index] > self.accepted_values[index]:
            #         line[index] = 1
            #         has_value = True
            #     # TODO : REDEFINE WHEN ACCEPTED AND WHEN NOT ACCEPTED

            if not has_value:
                m = max(line)
                for index, column in enumerate(line):
                    if column == m:
                        line[index] = 1

            for index, column in enumerate(line):
                if column < 1:
                    line[index] = 0
        return preds


if __name__ == "__main__":
    ml = MachineLearning()
    (x_train, y_train), (x_test, y_test) = ml.load_dataset()
    print(x_train.shape)
    print(y_train.shape)

    # model = ml.create_rnn_model()
    # model = ml.create_cnn_model()
    # model = ml.create_linear_model()
    model = ml.create_mlp_model()

    true_values = np.argmax(y_train, axis=1)
    x_train_preds = ml.round_preds(model.predict(x_train))

    # print("Confusion Train Matrix Before Training")
    # print(x_train_preds)
    # print(confusion_matrix(true_values, preds))

    true_values = np.argmax(y_test, axis=1)
    x_test_preds = ml.round_preds(model.predict(x_test))


    # print("Confusion Test Matrix Before Training")
    # print(x_test_preds)

    print('Before Train accuracy: {0}'.format(
        sklearn.metrics.accuracy_score(y_train, x_train_preds, normalize=True, sample_weight=None)))
    print('Before Test accuracy: {0}'.format(
        sklearn.metrics.accuracy_score(y_test, x_test_preds, normalize=True, sample_weight=None)))

    logs = model.fit(x_train, y_train, batch_size=100, epochs=10, verbose=1, validation_data=(x_test, y_test),
                     callbacks=[TensorBoard()])

    true_values = np.argmax(y_train, axis=1)
    x_train_preds = model.predict(x_train)

    print("Confusion Train Matrix After Training")
    print(x_train_preds)

    x_train_preds = ml.round_preds(x_train_preds)


    # print(confusion_matrix(true_values, preds))

    true_values = np.argmax(y_test, axis=1)
    x_test_preds = model.predict(x_test)

    print("Confusion Test Matrix After Training")
    print(x_test_preds)

    x_test_preds = ml.round_preds(x_test_preds)

    # print(confusion_matrix(true_values, preds))

    print('After Train accuracy: {0}'.format(
        sklearn.metrics.accuracy_score(y_train, x_train_preds, normalize=True, sample_weight=None)))

    print('After Test accuracy: {0}'.format(
        sklearn.metrics.accuracy_score(y_test, x_test_preds, normalize=True, sample_weight=None)))

    print('After Train hamming_loose: {0}'.format(
        sklearn.metrics.hamming_loss(y_train, x_train_preds)))

    print('After Test hamming_loose: {0}'.format(
        sklearn.metrics.hamming_loss(y_test, x_test_preds)))

    plt.plot(logs.history['accuracy'], label="train")
    plt.plot(logs.history['val_accuracy'], label="test")
    plt.legend(['Train', 'Val'], loc='upper left')
    plt.show()

    plt.plot(logs.history['loss'], label="train")
    plt.plot(logs.history['val_loss'], label="test")
    plt.legend(['Train', 'Val'], loc='upper left')
    plt.show()

    model.save('cnn.keras')